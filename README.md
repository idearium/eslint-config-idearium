# eslint-config-idearium

[![build status](https://gitlab.com/idearium/eslint-config-idearium/badges/master/build.svg)](https://gitlab.com/idearium/eslint-config-idearium/commits/master)
[![npm](https://img.shields.io/npm/v/@idearium/eslint-config.svg)](https://www.npmjs.com/package/@idearium/eslint-config)

## Installation

This package is available on [NPM](https://www.npmjs.com/package/@eslint/eslint-config):

  ```shell
  $ npm install @idearium/eslint-config
  ```
  ```javascript
  // @ .eslintrc
  // For node.js code (ES6).
  {
    "extends": "@idearium/eslint-config"
  }

  // For browser code (ES5).
  {
    "extends": "@idearium/eslint-config/src/browser"
  }
  ```
